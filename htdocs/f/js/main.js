function mobileCards() {
  var width = $(window).width();
  if (width < 580) {
      $('.viewmode-toggle .viewmode-toggle__grid').trigger('click');
  }
}


$(document).ready(function() {

 
    $('.mainpage-slider').slick({
      infinite: true,
      autoplay: true,
      autoplaySpeed: 5000,
      pauseOnHover: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      fade: true,
      speed: 900,
      cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
    });


    var mySwiper = new Swiper ('.swiper-container', {
      /*
      navigation: {
        nextEl: '.swiper-button-prev',
        prevEl: '.swiper-button-next',
      },
      */
      direction: 'horizontal',
      speed: 500,
      preventClicks: false,
      preventClicksPropagation: false,
      loop: true,
      loopedSlides: 3,
      slidesPerGroup: 3,
      loop: true,
      loopFillGroupWithBlank: true,
      freeMode: true,
      loopAdditionalSlides: true,
      slidesPerView: 3.8,
      spaceBetween: 0,
      touchRatio: 3,
      loopFillGroupWithBlank: false,
      observer: true,
      observeParents: true,
      observeSlideChildren: true,
      watchSlidesVisibility: true,
      breakpoints: {
        318: {
          slidesPerView: 1.5,
        },
        540: {
          slidesPerView: 2.2,
        },
        767: {
          slidesPerView: 2.8,
        },
        1080: {
          slidesPerView: 3.5,
        },
        1380: {
          slidesPerView: 4.5,
        }
      },
      
    });

    /*
    $('.section-links__slider').slick({
        infinite: true,
        centerMode: true,
        slidesToShow: 4.65,
        slidesToScroll: 1,
        prevArrow: '<div class="section-links__slider-before"><span></span></div>',
        nextArrow: '<div class="section-links__slider-after"><span></span></div>',
        responsive: [
          {
            breakpoint: 1380,
            settings: {
              slidesToShow: 3.65,
            }
          },
          {
            breakpoint: 1080,
            settings: {
              slidesToShow: 2.65,
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 1.65,
            }
          },
          {
            breakpoint: 540,
            settings: {
              slidesToShow: 1.1,
              centerMode: false,
              infinite: false
            }
          },
          
        ]
    }).slick('resize');

    */

    $('.cards-slider').slick({
      slidesToShow: 1.65,
      slidesToScroll: 1,
      arrows: true,
      infinite: true,
      centerMode: true,
      rows: 0,
    }).slick('resize');

  
    $(".js-smartPhoto").SmartPhoto({
    });


    $(document).on("mouseenter", ".section-offer__text-forhover_left", function() {
        $('.section-offer__text_left').addClass('hover');
    }).on("mouseout", ".section-offer__text-forhover_left", function() {
      $('.section-offer__text_left').removeClass('hover');
    });

    $(document).on("mouseenter", ".section-offer__text-forhover_right", function() {
      $('.section-offer__text_right').addClass('hover');
    }).on("mouseout", ".section-offer__text-forhover_right", function() {
      $('.section-offer__text_right').removeClass('hover');
    });

    $('.hamburger').on('click', function(e) {
      e.preventDefault();
      e.stopPropagation();
      $(this).toggleClass('is-active');
      $('body').toggleClass('modal-active');
      $('.mobile-menu').toggleClass('active');
    });

    $('.mobile-menu').on('click', function(e) {
      e.stopPropagation();
    });

    $('.header-top-menu__link').on('mouseover', function (e) {
      $(this).parent().find('.dropdown-menu').addClass('hover');
    });

    $('.header-top-menu__link').on('mouseleave', function (e) {
        $(this).parent().find('.dropdown-menu').removeClass('hover');
    });


    if (window.location.hash) {
      var item = window.location.hash;
      setTimeout(function(){
        $("html").scrollTop($(item).offset().top - 100);
      }, 500);
    }

    $('.dropdown-menu a').on('click', function(e) {
      setTimeout(function(){
        var top = $(window).scrollTop();
        $("html").scrollTop(top - 100);
      }, 500);
    });

    $('.page-header__submenu a:not(.sublevel)').on('click', function(e) {
      e.preventDefault();
      var target = $(this).attr('href');
      var headerHeight = $('.header').outerHeight();
      $('html,body').animate({
        scrollTop: $(target).offset().top - headerHeight
      });
    });

    $(document).on('click', '.grid-view .flipper', function () {
      $(this).find('.flipper-card').toggleClass('flipper-is-flipped');
    });

    $('.viewmode-toggle a').on('click', function(e) {
      e.preventDefault();
      $(this).parent().find('a').removeClass('active');
      $(this).addClass('active');
      if ($(this).attr('href') == 'grid') {
        $(this).parent().parent().next('.list-view').removeClass('list-view').addClass('grid-view').hide().fadeIn(300);
      } else {
        $(this).parent().parent().next('.grid-view').removeClass('grid-view').addClass('list-view').hide().fadeIn(300);
        $('.flipper-card').removeClass('flipper-is-flipped');
      }
    });


    // timetable

    $('.timetable .timetable-event').on('click', function(e) {
      e.preventDefault();
      e.stopPropagation();
      $('.timetable, .timetable-event').removeClass('active');
      $(this).addClass('active');
      $('.timetable').addClass('active');
    });

    $('.timetable .timetable-event a').on('click', function(e) {
      //e.stopPropagation();
    });


    $(document).on('click', '.cards-list.list-view .cards-list__item', function(e) {
      var link = $(this).find('.more-info').attr('href');
      window.location.href = link;
    });


    // show modal

    $('.show-modal').on('click', function(e) {
      e.preventDefault();
      e.stopPropagation();
      var target = $(this).attr('href');
      $('body').addClass('modal-active');
      $(target).fadeIn(300).addClass('modal_active');
      $('.modal-fade').fadeIn(300);
     
    });
  
    $('.modal__close').on('click', function(e) {
        e.preventDefault();
        $('body').removeClass('modal-active');
        $('.modal').fadeOut(300).removeClass('modal_active');
        $('.modal-fade').fadeOut(300);
    });

    $('.modal').on('click', function(e) {
      e.preventDefault();
      e.stopPropagation();
    });



    $('body').on('click', function(e) {
      $('.timetable, .timetable .timetable-event').removeClass('active');
      $('body').removeClass('modal-active');
      $('.modal').removeClass('modal_active');
      $('.modal-fade').fadeOut(300);
    });


  
  mobileCards();


  $('#feedback-form').on('submit',function(e) {
      e.preventDefault();
      if ( $(this).parsley().isValid() ) {
          $.ajax({
              url:'mail.php',
              data:$(this).serialize(),
              type:'POST',
              success:function(data){
                  $('#feedback-form').addClass('done');
              },
              error:function(data){
              }
          }); 
      }
  });

  

  $('.flipbook').booklet({
    width: '100%',
    height: '100%',
    pagePadding: 0,
  });

  const rippleElements = document.getElementsByClassName("btn");
  for(let i = 0; i < rippleElements.length; i++) {
    rippleElements[i].onclick = function(e){
      let X = e.pageX - this.offsetLeft;
      let Y = e.pageY - this.offsetTop;
      let rippleDiv = document.createElement("div");
      rippleDiv.classList.add('ripple');
      rippleDiv.setAttribute("style","top:"+Y+"px; left:"+X+"px;");
      let customColor = this.getAttribute('ripple-color');
      if (customColor) rippleDiv.style.background = customColor;
      this.appendChild(rippleDiv);
      setTimeout(function(){
        rippleDiv.parentElement.removeChild(rippleDiv);
      }, 900);
    }
  }

});




$(window).on('resize orientationchange', function () {
  //$('.section-links__slider').slick('resize');
  mobileCards();
});

